package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import employee.EmpBusinessLogic;
import employee.Employee;


public class BusinessTest {

	private static Employee employee;
	private static EmpBusinessLogic business;

	@Before
	public void setUp() {
		employee = new Employee();
		business = new EmpBusinessLogic();
		employee.setAge(45);
		employee.setMontlySalary(10000);

	}

	@Test
	public void testCalculateYearlySalary() {

		double salary = business.calculateYearlySalary(employee);
		assertEquals(120000, salary, 0.09);

	}
	

}