package employee;


public class EmpBusinessLogic {

	private static final int MONTHS_YEAR = 12;
	private static final int APPRAISAL_AMOUNT = 10000;

	public double calculateYearlySalary(Employee employee) {
		return employee.getMontlySalary() * MONTHS_YEAR;
	}

	public double calculateAppraisal(Employee employee) {
		double appraisal = 0;
		if (employee.getMontlySalary() < APPRAISAL_AMOUNT) {
			appraisal = 500;
		}

		else {
			appraisal = 1000;
		}
		return appraisal;
	}
}
		    