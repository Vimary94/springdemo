package employee;

public class Employee {
	private String nombre;
	private double montlySalary;
	private int age;

	public Employee() {
		super();
	}

	public Employee(String nombre, double montlySalary, int age) {
		super();
		this.nombre = nombre;
		this.montlySalary = montlySalary;
		this.age = age;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getMontlySalary() {
		return montlySalary;
	}

	public void setMontlySalary(double montlySalary) {
		this.montlySalary = montlySalary;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
